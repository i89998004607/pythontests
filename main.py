import time
from selenium import webdriver

driver = webdriver.Chrome('/Users/kvashnin/Documents/Projects/selenium-parcing/chromedriver') 

driver.get('https://yandex.ru/'); 
assert "Яндекс" in driver.title

def openSberInYandex():
    driver.find_element_by_xpath('//*[@id="text"]').send_keys('пао сбербанк сайт официальный')
    driver.find_element_by_css_selector('button.mini-suggest__button').click()
    driver.find_element_by_xpath('//*[@id="search-result"]/li[1]/div/div[1]/h2/a/div[2]').click()
    time.sleep(5)    

def assertSberWebSite():
    driver.switch_to.window(driver.window_handles[1])
    assert "Частным клиентам — СберБанк" in driver.title
    attr = driver.find_element_by_xpath('//*[@id="main-page"]/div[1]/div/div[3]/header/div/a/img').get_attribute('alt')
    assert attr == 'Официальный сайт Сбербанка России'

openSberInYandex()
assertSberWebSite()

